Language: 	Java 14
IDE: 		Eclipse
Framework:	Spring-boot MVC structure
ORM: 		Hibernate
Database:	SQLite
Server:		Tomcat 9

This is the basic example of Spring-boot MVC structure to become restful server. You can see the "setup guide.docx" to reference.