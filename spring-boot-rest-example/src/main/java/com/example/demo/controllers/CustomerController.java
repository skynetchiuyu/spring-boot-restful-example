package com.example.demo.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Customer;
import com.example.demo.repositories.CustomerRepository;
import com.example.demo.services.CustomerService;

// This is http CRUD Controller for Customer 

@RestController
@RequestMapping("/customers")
public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	
	@PostMapping("/add")
	public Customer add(@RequestBody Customer customer) {
		return customerService.create(customer);
	}

	
	@GetMapping("/{id}")
	public Optional<Customer> getCustomerById(@PathVariable String id) {
		return customerService.getCustomer(id);
	}
	
	
	@GetMapping("")
	public Iterable<Customer> list() {
		return customerService.getCustomers();
	}
}
