package com.example.demo.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.entities.Customer;

public interface CustomerRepository extends CrudRepository<Customer, String> {

}
