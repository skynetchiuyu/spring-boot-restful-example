package com.example.demo.services;

import java.util.Optional;

import com.example.demo.entities.Customer;

public interface CustomerService {

	public Iterable<Customer> getCustomers();
	
	public Optional<Customer> getCustomer(String id);
	
	public Customer create(Customer customer);
	
	public Customer update(Customer customer);
	
	public Customer delete(String id);
	
	public Customer delete(Customer customer);
}
