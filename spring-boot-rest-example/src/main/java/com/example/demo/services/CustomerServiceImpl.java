package com.example.demo.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.entities.Customer;
import com.example.demo.repositories.CustomerRepository;

public class CustomerServiceImpl implements CustomerService {

	@Autowired
    private CustomerRepository customerRepository;
	
	@Override
	public Iterable<Customer> getCustomers() {
		return customerRepository.findAll();
	}
	
	@Override
	public Optional<Customer> getCustomer(String id) {
		return customerRepository.findById(id);
	}

	@Override
	public Customer create(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public Customer update(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public Customer delete(String id) {
		Customer customer = customerRepository.findById(id).get();
		if(customer != null) {
			customer.setEnable(false);
		}
		return customerRepository.save(customer);
	}

	@Override
	public Customer delete(Customer customer) {
		customer.setEnable(false);
		return customerRepository.save(customer);
	}

}
